// // const data = require("./pokeData")
// //     .then(datas => console.log(datas))

// const fetch = require('node-fetch');


// async function pokemon() {
//     const result = await fetch('https://pokeapi.co/api/v2/pokemon?limit=1000s&offset=0')
//         .then(res => res.json())
//     console.log(result.results[1])
//     // .then(res => {
//     //     console.log(res)
//     // })
// }

const fetch = require('node-fetch');


async function pokemon() {
    const result = await fetch('https://pokeapi.co/api/v2/pokemon?limit=1000&offset=0')
        .then(res => res.json());

    for (let i = 0; i < 3; i++) {
        const index = Math.floor(Math.random() * (1118 - 1 + 1) + 1);//inclusive random
        const pokemonName_Url = result.results[index];//gets the pokemon name and url
        //console.log(pokemonName_Url)
        const pokemonName = pokemonName_Url.name;//pokemon name
        const pokemonUrl = await fetch(pokemonName_Url.url)//pokemon url give the data of that pokemon
            .then(res => res.json());
        // console.log(pokemonUrl)
        //console.log(pokemonUrl.species)
        //console.log(pokemonUrl.species.url)
        const species = await fetch(pokemonUrl.species.url)//pokemon data=>species data=>url
            .then(res => res.json());
        // species.flavor_text_entries.forEach(ele=> {
        //     console.log(ele.language.name);
        //     console.log(ele.language.url);

        //   })

        for (let i = 0; i < species.flavor_text_entries.length; i++) {
            if (species.flavor_text_entries[i].language.name == 'en') {
                flavor_text = species.flavor_text_entries[i].flavor_text;
                break;
            }
        }

        console.log(`${pokemonName}: ${flavor_text}`)
        console.log("\n")

    }



}


pokemon()

