const fetch = require("node-fetch")
const cardsLink = 'https://deckofcardsapi.com/api/deck/new/draw/?count=1'
const deckOfCards = async () => {
    let shuffle = []
    await fetch(cardsLink)
        .then(res => res.json())
        .then(data => {
            //console.log(data.deck_id)
            shuffle.push(data.cards[0].value + " " + "of" + " " + data.cards[0].suit)
            let link = "https://deckofcardsapi.com/api/deck/" + `${data.deck_id}/draw/?count=1`
            //return fetch("https://deckofcardsapi.com/api/deck/qinr5c50kbyz/shuffle/")
            return fetch(link)
        })
        .then(res => res.json())
        .then(data => {
            // console.log(data.deck_id)
            shuffle.push(data.cards[0].value + " " + "of" + " " + data.cards[0].suit)
        })
        .catch(error => console.log(error))
    console.log(shuffle)
}
deckOfCards()